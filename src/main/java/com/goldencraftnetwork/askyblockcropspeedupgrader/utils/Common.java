package com.goldencraftnetwork.askyblockcropspeedupgrader.utils;

import com.goldencraftnetwork.askyblockcropspeedupgrader.listeners.CropSpeedUpgraderGui;
import com.goldencraftnetwork.askyblockcropspeedupgrader.models.ASkyBlockCropSpeedUpgraderConfig;
import com.wasteofplastic.askyblock.ASkyBlockAPI;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandMap;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.lang.reflect.Field;

public class Common {

    private static Plugin plugin;
    private static ASkyBlockCropSpeedUpgraderConfig configObject;
    private static CropSpeedUpgraderGui cropSpeedUpgraderGui;
    private static ASkyBlockAPI skyBlockAPI;
    private static File islandDataFile;
    private static YamlConfiguration islandData;

    public static void registerCommand(final BukkitCommand command) {
        try {
            final Field commandMapField = Bukkit.getServer().getClass().getDeclaredField("commandMap");
            commandMapField.setAccessible(true);

            final CommandMap commandMap = (CommandMap) commandMapField.get(Bukkit.getServer());
            commandMap.register(command.getLabel(), command);
        } catch (final Exception exception) {
            exception.printStackTrace();
        }
    }

    private static Plugin getPlugin() {
        return Common.plugin;
    }

    public static void setPlugin(final Plugin plugin) {
        Common.plugin = plugin;
    }

    public static CropSpeedUpgraderGui getCropSpeedUpgraderGui() {
        return Common.cropSpeedUpgraderGui;
    }

    private static void setCropSpeedUpgraderGui(final CropSpeedUpgraderGui cropSpeedUpgraderGui) {
        Common.cropSpeedUpgraderGui = cropSpeedUpgraderGui;
    }

    public static ASkyBlockCropSpeedUpgraderConfig getConfigObject() {
        return Common.configObject;
    }

    public static ASkyBlockAPI getSkyBlockAPI() {
        return Common.skyBlockAPI;
    }

    private static void setSkyBlockAPI(final ASkyBlockAPI skyBlockAPI) {
        Common.skyBlockAPI = skyBlockAPI;
    }

    public static YamlConfiguration getIslandData() {
        return Common.islandData;
    }

    private static void setIslandData(final YamlConfiguration islandData) {
        Common.islandData = islandData;
    }

    public static File getIslandDataFile() {
        return Common.islandDataFile;
    }

    private static void setIslandDataFile(final File islandDataFile) {
        Common.islandDataFile = islandDataFile;
    }

    public static void reloadConfigObject() {
        Common.plugin.reloadConfig();
        Common.configObject = new ASkyBlockCropSpeedUpgraderConfig();
        Common.configObject.buildConfig(Common.getPlugin().getConfig());
    }

    public static void initialize() {
        Common.setCropSpeedUpgraderGui(new CropSpeedUpgraderGui());
        Common.setSkyBlockAPI(ASkyBlockAPI.getInstance());
        Common.setIslandDataFile(new File("plugins/ASkyBlockCropSpeedUpgrader", "upgrades.yml"));
        Common.setIslandData(YamlConfiguration.loadConfiguration(Common.getIslandDataFile()));
    }

}
