package com.goldencraftnetwork.askyblockcropspeedupgrader;

import com.goldencraftnetwork.askyblockcropspeedupgrader.commands.ReloadPluginCommand;
import com.goldencraftnetwork.askyblockcropspeedupgrader.utils.Common;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;

public class ASkyBlockCropSpeedUpgrader extends JavaPlugin {

    @Override
    public void onEnable() {
        super.onEnable();
        Common.setPlugin(this);
        Common.initialize();
        this.registerCommands();
        this.registerEvents();
        this.saveDefaultConfig();
        Common.reloadConfigObject();

    }

    @Override
    public void onDisable() {
        super.onDisable();
        try {
            Common.getIslandData().save(Common.getIslandDataFile());
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }

    private void registerEvents() {
        this.getServer().getPluginManager().registerEvents(Common.getCropSpeedUpgraderGui(), this);
    }

    private void registerCommands() {
        Common.registerCommand(new ReloadPluginCommand());
    }

}
