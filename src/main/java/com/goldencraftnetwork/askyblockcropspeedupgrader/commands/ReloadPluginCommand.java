package com.goldencraftnetwork.askyblockcropspeedupgrader.commands;

import com.goldencraftnetwork.askyblockcropspeedupgrader.utils.Common;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class ReloadPluginCommand extends BukkitCommand {

    public ReloadPluginCommand() {
        super("cropspeedupgrader");
        this.setAliases(Arrays.asList("csu", "cropspeed"));
        this.setDescription("The ASkyBlockCropSpeedUpgrader command");
    }

    @Override
    public boolean execute(final CommandSender commandSender, final String commandLabel, final String[] args) {
        if (commandSender.isOp() && args.length > 0 && args[0].equals("reload")) {
            Common.reloadConfigObject();
            commandSender.sendMessage("Config reloaded");
        } else if (args.length > 0 && args[0].equals("upgrade")) {
            Common.getCropSpeedUpgraderGui().openInventory(((Player) commandSender));
        } else {
            return false;
        }
        return true;
    }
}
