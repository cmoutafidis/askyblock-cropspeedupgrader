package com.goldencraftnetwork.askyblockcropspeedupgrader.listeners;

import com.earth2me.essentials.api.Economy;
import com.earth2me.essentials.api.NoLoanPermittedException;
import com.earth2me.essentials.api.UserDoesNotExistException;
import com.goldencraftnetwork.askyblockcropspeedupgrader.utils.Common;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockGrowEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;

public class CropSpeedUpgraderGui implements InventoryHolder, Listener {

    private final Inventory inventory;

    public CropSpeedUpgraderGui() {
        this.inventory = Bukkit.createInventory(this, 36, "Crop Speed Upgrader");
    }

    @Override
    public Inventory getInventory() {
        return this.inventory;
    }

    private void initializeItems(final Location location) {

        this.inventory.clear();
        this.inventory.setItem(12, this.getItemStack(Common.getIslandData().getInt(this.getPathFromLocation(location) + "." + Material.WHEAT.name()), Common.getConfigObject().getWheatCosts(), Material.WHEAT, "Wheat"));
        this.inventory.setItem(13, this.getItemStack(Common.getIslandData().getInt(this.getPathFromLocation(location) + "." + Material.CARROT_ITEM.name()), Common.getConfigObject().getCarrotCosts(), Material.CARROT_ITEM, "Carrot"));
        this.inventory.setItem(14, this.getItemStack(Common.getIslandData().getInt(this.getPathFromLocation(location) + "." + Material.POTATO_ITEM.name()), Common.getConfigObject().getPotatoCosts(), Material.POTATO_ITEM, "Potato"));
        this.inventory.setItem(21, this.getItemStack(Common.getIslandData().getInt(this.getPathFromLocation(location) + "." + Material.MELON.name()), Common.getConfigObject().getMelonCosts(), Material.MELON, "Melon"));
        this.inventory.setItem(22, this.getItemStack(Common.getIslandData().getInt(this.getPathFromLocation(location) + "." + Material.PUMPKIN.name()), Common.getConfigObject().getPumpkinCosts(), Material.PUMPKIN, "Pumpkin"));
        this.inventory.setItem(23, this.getItemStack(Common.getIslandData().getInt(this.getPathFromLocation(location) + "." + Material.NETHER_STALK.name()), Common.getConfigObject().getNetherWartCosts(), Material.NETHER_STALK, "Nether Wart"));
    }

    private ItemStack getItemStack(final int cropLevel, final ArrayList<Integer> costs, final Material material, final String materialName) {
        return this.createGuiItem(material, materialName, "Current grow speed level is " + cropLevel + ".", cropLevel != costs.size() ? "Left click to level up once for " + costs.get(cropLevel) + "$." : "Crop seed already MAX level.");
    }

    private ItemStack createGuiItem(final Material material, final String title, final String... lore) {
        final ItemStack item = new ItemStack(material, 1);
        final ItemMeta meta = item.getItemMeta();

        meta.setDisplayName(title);
        meta.setLore(Arrays.asList(lore));
        item.setItemMeta(meta);
        return item;
    }

    public void openInventory(final HumanEntity entity) {
        this.initializeItems(Common.getSkyBlockAPI().getIslandAt(entity.getLocation()).getCenter());
        entity.openInventory(this.inventory);
    }

    @EventHandler
    public void onBlockGrowEvent(final BlockGrowEvent event) {
        int maxAge = 0;
        int speedLevel = 0;
        int type = 0;
        if (Material.CROPS.name().equals(event.getNewState().getType().name())) {
            type = 59;
            maxAge = 7;
            speedLevel = Common.getIslandData().getInt(this.getPathFromLocation(Common.getSkyBlockAPI().getIslandAt(event.getBlock().getLocation()).getCenter()) + "." + Material.WHEAT);
        } else if (Material.CARROT.name().equals(event.getNewState().getType().name())) {
            type = 141;
            maxAge = 7;
            speedLevel = Common.getIslandData().getInt(this.getPathFromLocation(Common.getSkyBlockAPI().getIslandAt(event.getBlock().getLocation()).getCenter()) + "." + Material.CARROT_ITEM);
        } else if (Material.POTATO.name().equals(event.getNewState().getType().name())) {
            type = 142;
            maxAge = 7;
            speedLevel = Common.getIslandData().getInt(this.getPathFromLocation(Common.getSkyBlockAPI().getIslandAt(event.getBlock().getLocation()).getCenter()) + "." + Material.POTATO_ITEM);
        } else if (Material.MELON_STEM.name().equals(event.getNewState().getType().name())) {
            type = 105;
            maxAge = 7;
            speedLevel = Common.getIslandData().getInt(this.getPathFromLocation(Common.getSkyBlockAPI().getIslandAt(event.getBlock().getLocation()).getCenter()) + "." + Material.MELON);
        } else if (Material.PUMPKIN_STEM.name().equals(event.getNewState().getType().name())) {
            type = 104;
            maxAge = 7;
            speedLevel = Common.getIslandData().getInt(this.getPathFromLocation(Common.getSkyBlockAPI().getIslandAt(event.getBlock().getLocation()).getCenter()) + "." + Material.PUMPKIN);
        } else if (Material.NETHER_WARTS.name().equals(event.getNewState().getType().name())) {
            type = 115;
            maxAge = 3;
            speedLevel = Common.getIslandData().getInt(this.getPathFromLocation(Common.getSkyBlockAPI().getIslandAt(event.getBlock().getLocation()).getCenter()) + "." + Material.NETHER_STALK);
        } else {
            return;
        }
        final String metaAgeString = event.getBlock().getState().getData().toString().split("\\(")[1];
        final int metaAge = Integer.parseInt(metaAgeString.substring(0, metaAgeString.length() - 1));

        int newAge = metaAge + speedLevel + 1;
        if (newAge > maxAge) {
            newAge = maxAge;
        }
        event.getNewState().setData(new MaterialData(type, (byte) newAge));
    }

    @EventHandler
    public void onInventoryClick(final InventoryClickEvent event) {
        if (event.getInventory().getHolder() != this) {
            return;
        }

        if (event.getRawSlot() < 36 && !event.getAction().name().equals("PICKUP_ALL")) {
            event.setCancelled(true);
            return;
        }

        if (event.getRawSlot() > 35) {
            return;
        }

        event.setCancelled(true);
        final ItemStack clickedItem = event.getCurrentItem();
        if (clickedItem == null || clickedItem.getType().equals(Material.AIR)) {
            return;
        }

        final Player player = (Player) event.getWhoClicked();
        final int newLevel = this.upgradeCrop(player, Common.getSkyBlockAPI().getIslandAt(player.getLocation()).getCenter(), clickedItem.getType());
        switch (newLevel) {
            case -2:
                player.sendMessage("Unhandled Exception, please contact an admin.");
                break;
            case -1:
                player.sendMessage("Insufficient balance to upgrade " + clickedItem.getType().name() + ".");
                break;
            case 0:
                player.sendMessage("Cannot upgrade " + clickedItem.getType().name() + ": already max level.");
                break;
            default:
                player.sendMessage("Crop speed for " + clickedItem.getType().name() + " was upgraded to level " + newLevel + ".");
                this.initializeItems(Common.getSkyBlockAPI().getIslandAt(player.getLocation()).getCenter());
                break;
        }
    }

    private int upgradeCrop(final Player player, final Location islandLocation, final Material cropType) {
        final int newLevel = Common.getIslandData().getInt(this.getPathFromLocation(islandLocation) + "." + cropType.name()) + 1;
        BigDecimal cost = new BigDecimal(0);
        try {
            if (Material.WHEAT.name().equals(cropType.name())) {
                cost = BigDecimal.valueOf(Common.getConfigObject().getWheatCosts().get(newLevel - 1));
            } else if (Material.CARROT_ITEM.name().equals(cropType.name())) {
                cost = BigDecimal.valueOf(Common.getConfigObject().getCarrotCosts().get(newLevel - 1));
            } else if (Material.POTATO_ITEM.name().equals(cropType.name())) {
                cost = BigDecimal.valueOf(Common.getConfigObject().getPotatoCosts().get(newLevel - 1));
            } else if (Material.MELON.name().equals(cropType.name())) {
                cost = BigDecimal.valueOf(Common.getConfigObject().getMelonCosts().get(newLevel - 1));
            } else if (Material.PUMPKIN.name().equals(cropType.name())) {
                cost = BigDecimal.valueOf(Common.getConfigObject().getPumpkinCosts().get(newLevel - 1));
            } else if (Material.NETHER_STALK.name().equals(cropType.name())) {
                cost = BigDecimal.valueOf(Common.getConfigObject().getNetherWartCosts().get(newLevel - 1));
            } else {
                return 0;
            }

            if (Economy.hasEnough(player.getName(), cost)) {
                Economy.substract(player.getName(), cost);
            } else {
                return -1;
            }
        } catch (final IndexOutOfBoundsException exception) {
            return 0;
        } catch (final UserDoesNotExistException exception) {
            exception.printStackTrace();
            return -2;
        } catch (final NoLoanPermittedException exception) {
            exception.printStackTrace();
            return -2;
        }

        Common.getIslandData().set(this.getPathFromLocation(islandLocation) + "." + cropType.name(), newLevel);
        return newLevel;
    }

    private String getPathFromLocation(final Location location) {
        return location.getBlock().getX() + "-" + location.getBlock().getY() + "-" + location.getBlock().getZ();
    }

}
