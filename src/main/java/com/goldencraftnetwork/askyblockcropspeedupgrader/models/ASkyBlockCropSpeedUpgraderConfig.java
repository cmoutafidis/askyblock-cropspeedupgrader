package com.goldencraftnetwork.askyblockcropspeedupgrader.models;

import org.bukkit.configuration.file.FileConfiguration;

import java.util.ArrayList;

public class ASkyBlockCropSpeedUpgraderConfig {

    private ArrayList<Integer> wheatCosts;
    private ArrayList<Integer> carrotCosts;
    private ArrayList<Integer> potatoCosts;
    private ArrayList<Integer> melonCosts;
    private ArrayList<Integer> pumpkinCosts;
    private ArrayList<Integer> netherWartCosts;

    public ASkyBlockCropSpeedUpgraderConfig() {
        this.wheatCosts = new ArrayList<Integer>();
        this.carrotCosts = new ArrayList<Integer>();
        this.potatoCosts = new ArrayList<Integer>();
        this.melonCosts = new ArrayList<Integer>();
        this.pumpkinCosts = new ArrayList<Integer>();
        this.netherWartCosts = new ArrayList<Integer>();
    }

    public void buildConfig(final FileConfiguration fileConfiguration) {
        this.wheatCosts = new ArrayList<Integer>(fileConfiguration.getIntegerList("wheat"));
        this.carrotCosts = new ArrayList<Integer>(fileConfiguration.getIntegerList("carrot_item"));
        this.potatoCosts = new ArrayList<Integer>(fileConfiguration.getIntegerList("potato_item"));
        this.melonCosts = new ArrayList<Integer>(fileConfiguration.getIntegerList("melon"));
        this.pumpkinCosts = new ArrayList<Integer>(fileConfiguration.getIntegerList("pumpkin"));
        this.netherWartCosts = new ArrayList<Integer>(fileConfiguration.getIntegerList("nether_stalk"));
    }

    public ArrayList<Integer> getWheatCosts() {
        return this.wheatCosts;
    }

    public void setWheatCosts(final ArrayList<Integer> wheatCosts) {
        this.wheatCosts = wheatCosts;
    }

    public ArrayList<Integer> getCarrotCosts() {
        return this.carrotCosts;
    }

    public void setCarrotCosts(final ArrayList<Integer> carrotCosts) {
        this.carrotCosts = carrotCosts;
    }

    public ArrayList<Integer> getPotatoCosts() {
        return this.potatoCosts;
    }

    public void setPotatoCosts(final ArrayList<Integer> potatoCosts) {
        this.potatoCosts = potatoCosts;
    }

    public ArrayList<Integer> getMelonCosts() {
        return this.melonCosts;
    }

    public void setMelonCosts(final ArrayList<Integer> melonCosts) {
        this.melonCosts = melonCosts;
    }

    public ArrayList<Integer> getPumpkinCosts() {
        return this.pumpkinCosts;
    }

    public void setPumpkinCosts(final ArrayList<Integer> pumpkinCosts) {
        this.pumpkinCosts = pumpkinCosts;
    }

    public ArrayList<Integer> getNetherWartCosts() {
        return this.netherWartCosts;
    }

    public void setNetherWartCosts(final ArrayList<Integer> netherWartCosts) {
        this.netherWartCosts = netherWartCosts;
    }
}
